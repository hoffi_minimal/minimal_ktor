package main

import com.hoffi.ktor.cli.App
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.serialization.ExperimentalSerializationApi

@ExperimentalCoroutinesApi
@OptIn(ExperimentalSerializationApi::class)
fun main(args: Array<String>) {
    println("Hello, classic JVM! in ${System.getProperty("user.dir")}")
    App().main(args)
}
