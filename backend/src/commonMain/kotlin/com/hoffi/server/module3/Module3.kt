package com.hoffi.server.module3

import com.hoffi.ktor.apiModel.ApiData
import com.hoffi.ktor.apiRoutes.module3.Module3Routes
import io.ktor.server.application.*
import io.ktor.server.resources.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kotlinx.datetime.Clock
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime
import kotlinx.serialization.ExperimentalSerializationApi

@ExperimentalSerializationApi
fun Application.module3() {
    routing {
        get<Module3Routes> {
            call.respond("please call /module3/data")
        }
        get<Module3Routes.Data> {
            val apiData = ApiData("hardcoded", Clock.System.now().toLocalDateTime(TimeZone.currentSystemDefault()))
            call.respond(apiData)
        }
    }
}
