plugins {
    kotlin("multiplatform")
    kotlin("plugin.serialization")
    application
    id("com.github.johnrengelman.shadow")
}

group = "com.hoffi"
version = "1.0.0"
val artifactName by extra { "${rootProject.name.toLowerCase()}-${project.name.toLowerCase()}" }
val theMainClass by extra { "main/MainKt" }


kotlin {
    jvmToolchain(BuildSrcGlobal.jdkVersion)
    val hostOs = System.getProperty("os.name")
    val isMingwX64 = hostOs.startsWith("Windows")
    println("host operating system: \"$hostOs\" (isMingwX64:${isMingwX64})")
    val nativeTarget = when {
        hostOs == "Mac OS X" -> macosX64("native")
        hostOs == "Linux" -> linuxX64("native")
        isMingwX64 -> mingwX64("native")
        else -> throw GradleException("Host OS is not supported in Kotlin/Native.")
    }

    nativeTarget.apply {
        binaries {
            executable {
                entryPoint = "main.main"
            }
        }
    }
    jvm {
        compilations.all {
            kotlinOptions.jvmTarget = BuildSrcGlobal.JavaLanguageVersion.toString()
        }
        // withJava()
        testRuns["test"].executionTask.configure {
            useJUnitPlatform()
        }
    }
    sourceSets {
        val commonMain by getting  { // predefined by gradle multiplatform plugin
            dependencies {
                implementation(project(":apiModel"))
                implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
                //implementation("io.github.microutils:kotlin-logging:2.0.6")
                implementation("com.squareup.okio:okio".depAndVersion())
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json".depAndVersion())
                implementation("org.jetbrains.kotlinx:kotlinx-datetime".depAndVersion())

                implementation("io.ktor:ktor-server-core".depAndVersion())
                implementation("io.ktor:ktor-server-cio".depButVersionOf("io.ktor:ktor-server-core"))
                implementation("io.ktor:ktor-server-resources".depButVersionOf("io.ktor:ktor-server-core"))
                implementation("io.ktor:ktor-server-content-negotiation".depButVersionOf("io.ktor:ktor-server-core"))
                implementation("io.ktor:ktor-server-http-redirect".depButVersionOf("io.ktor:ktor-server-core"))
                implementation("io.ktor:ktor-server-forwarded-header".depButVersionOf("io.ktor:ktor-server-core"))
                implementation("io.ktor:ktor-serialization-kotlinx-json".depButVersionOf("io.ktor:ktor-server-core"))
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test")) // This brings all the platform dependencies automatically
                implementation("io.ktor:ktor-server-test-host".depButVersionOf("io.ktor:ktor-server-core"))
                //implementation("org.jetbrains.kotlin:kotlin-test-junit:${Deps.Ktor.version}")
            }
        }
        val nativeMain by getting {
            dependencies {
            }
        }
        val nativeTest by getting
        val jvmMain by getting {
            dependencies {
                implementation("ch.qos.logback:logback-classic".depAndVersion())
                implementation("io.ktor:ktor-server-netty".depButVersionOf("io.ktor:ktor-server-core"))
                implementation("io.ktor:ktor-network-tls-certificates".depButVersionOf("io.ktor:ktor-server-core"))
            }
        }
        val jvmTest by getting {
            dependencies {
                implementation(kotlin("test"))
            }
        }
    }
}

application {
    mainClass.set(theMainClass)
    tasks.run.get().workingDir = rootProject.projectDir
    applicationDefaultJvmArgs = listOf("-Dio.ktor.development=true")
}

tasks {
    named<JavaExec>("run") {
        standardInput = System.`in`
        classpath += objects.fileCollection().from(
            named("compileKotlinJvm"),
            configurations.named("jvmRuntimeClasspath")
        )
        classpath += files("$buildDir/processedResources/jvm/main")
    }
    shadowJar {
        manifest { attributes["Main-Class"] = theMainClass }
        archiveClassifier.set("fat")
        archiveBaseName.set(artifactName)
        val jvmJar = named<org.gradle.jvm.tasks.Jar>("jvmJar").get()
        from(jvmJar.archiveFile)
        configurations.add(project.configurations.named("jvmRuntimeClasspath").get())
    }
}

val jar by tasks.existing {
    val shadowJar by tasks.existing
    dependsOn(shadowJar)
}
val exe by tasks.registering {
    val nativeBinaries by tasks.existing
    dependsOn(nativeBinaries)
}

// Helper tasks to speed up things and don't waste time
//=====================================================
// 'c'ompile 'c'ommon
val cc by tasks.registering {
    dependsOn(":compileKotlinJvm", ":compileTestKotlinJvm")
}
val ca by tasks.registering {
    dependsOn(cc)
    dependsOn(":compileKotlinNative", ":compileTestKotlinNative")
}

// ################################################################################################
// #####    pure informational stuff on stdout    #################################################
// ################################################################################################
tasks.register<CheckVersionsTask>("checkVersions") { // implemented in buildSrc/src/main/kotlin/CheckVersionsTask.kt
    scope = "ALL"
}
tasks.register("printClasspath") {
    group = "misc"
    description = "print classpath"
    doLast {
        //project.getConfigurations().filter { it.isCanBeResolved }.forEach {
        //    println(it.name)
        //}
        //println()
        val targets = listOf(
            "metadataCommonMainCompileClasspath",
            "commonMainApiDependenciesMetadata",
            "commonMainImplementationDependenciesMetadata",
            "jvmCompileClasspath",
            "kotlinCompilerClasspath"
        )
        targets.forEach { targetConfiguration ->
            println("$targetConfiguration:")
            println("=".repeat("$targetConfiguration:".length))
            project.getConfigurations()
                .getByName(targetConfiguration).files
                // filters only existing and non-empty dirs
                .filter { (it.isDirectory() && it.listFiles().isNotEmpty()) || it.isFile() }
                .forEach { println(it) }
        }
    }
}
