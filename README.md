# minimal_ktor

example of a minimal pure kotlin multiplatform-project (mpp) of an embedded ktor web-server.

## tl;dr

beware to have the jvm HeapSpace big enough!

see `<root>/gradle.properties`

```properties
org.gradle.jvmargs=-Xmx8048M
```

## limitations

currently ktor does not support mingwX64 windows native target.

currently ktor does not support TLS (https) for native Multi-Platform targets (only for JVM)

## prerequisites

For https/tls/ssl you need a java keystore with a valid certificate</br>
in the local directory `./keystore.jks`

You can generate a self-signed certificate e.g. via java's keytool

Currently, the keystore password is hardcoded to 'password'. 

```bash
keytool -keystore ./keystore.jks -alias minimalKtorAlias \
    -genkeypair -keyalg RSA -keysize 4096 -validity 5000 \
    -dname 'CN=localhost, OU=ktor, O=ktor, L=Unspecified, ST=Unspecified, C=DE' \
    -ext 'SAN:c=DNS:localhost,IP:127.0.0.1'
```

## build

```bash
./gradlew build
```

beware to have the jvm HeapSpace big enough!

see `<root>/gradle.properties`

```properties
org.gradle.jvmargs=-Xmx8048M
```

### compile only

```bash
./gradlew cc # (cc = common compile) (implicitly calls compileKotlinJvm compileTestKotlinJvm)
```

### build executable fat jar (build/libs/*fat.jar)

```bash
./gradlew jar # implicitly calls shadowJar
```

### run only

```bash
./gradlew run
```

### native binaries only (build/bin/native/*/minimal_ktor.kexe)

```bash
./gradlew exe # implicitly calls nativeBinaries
```

## terminal client calling the server

either:

```bash
curl localhost:8080/module1
curl -k https://localhost:8443/module1
```

or:

extract the certificate from keystore in pem format:

```bash
keytool -exportcert -alias minimalKtorAlias -keypass password \
        -keystore ./keystore.jks -storepass password -rfc -file minimalKtor.pem
```

```bash
curl --cacert ./minimalKtor.pem https://localhost:8443/module1
```

<!--page layout css styles for markup headers numbering -->
<style type="text/css"> /* automatic heading numbering */ h1 { counter-reset: h2counter; font-size: 24pt; } h2 { counter-reset: h3counter; font-size: 22pt; margin-top: 2em; } h3 { counter-reset: h4counter; font-size: 16pt; } h4 { counter-reset: h5counter; font-size: 14pt; } h5 { counter-reset: h6counter; } h6 { } h2:before { counter-increment: h2counter; content: counter(h2counter) ".  "; } h3:before { counter-increment: h3counter; content: counter(h2counter) "." counter(h3counter) ".  "; } h4:before { counter-increment: h4counter; content: counter(h2counter) "." counter(h3counter) "." counter(h4counter) ".  "; } h5:before { counter-increment: h5counter; content: counter(h2counter) "." counter(h3counter) "." counter(h4counter) "." counter(h5counter) ".  "; } h6:before { counter-increment: h6counter; content: counter(h2counter) "." counter(h3counter) "." counter(h4counter) "." counter(h5counter) "." counter(h6counter) ".  "; } </style>
