package com.hoffi.ktor.apiRoutes.module3

import io.ktor.resources.*
import kotlinx.serialization.Serializable

/** type-safe route definitions */
@Serializable
@Resource("/module3")
class Module3Routes {
    /** this is the ApiData Resource (=path), NOT the apiModel class ApiData !!! */
    @Serializable
    @Resource("data")
    data class Data(val parent: Module3Routes = Module3Routes())
}
