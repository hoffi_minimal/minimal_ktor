package com.hoffi.ktor.cli.json

import kotlinx.serialization.Serializable

/** created with https://plugins.jetbrains.com/plugin/9960-json-to-kotlin-class-jsontokotlinclass- */
@Serializable
data class IpInfo(
    val city: String = "",
    val country: String = "",
    val hostname: String = "",
    val ip: String = "",
    val loc: String = "",
    val org: String = "",
    val postal: String = "",
    val readme: String = "",
    val region: String = "",
    val timezone: String = ""
)
