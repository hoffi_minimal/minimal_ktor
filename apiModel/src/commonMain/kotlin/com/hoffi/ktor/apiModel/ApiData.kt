package com.hoffi.ktor.apiModel

import com.hoffi.ktor.env.EnvInfo
import com.hoffi.ktor.json.serializers.LocalDateTimeSerializer
import kotlinx.datetime.LocalDateTime
import kotlinx.serialization.EncodeDefault
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.Serializable

@ExperimentalSerializationApi
@Serializable
data class ApiData(
    val name: String,
    @Serializable(with = LocalDateTimeSerializer::class)
    val createdAt: LocalDateTime,
    @EncodeDefault
    val serverType: String = "${EnvInfo.exeType()}|${EnvInfo.osName()}|${EnvInfo.cpuArch()}"
)
