package main

import io.ktor.server.cio.*
import io.ktor.server.engine.*
import kotlinx.serialization.ExperimentalSerializationApi

/**
 * as of 06/2022 ktor version 2.0.2
 *   https://ktor.io/docs/native-server.html
 *     - for native ktor server only the CIO engine is supported
 *     - HTTPS without a reverse proxy is not supported
 *     - Windows target is not supported
 */
@ExperimentalSerializationApi
fun main() {
    println("Hello, Kotlin/Native!")
    embeddedServer(CIO, port = 8080, module = com.hoffi.server.loadModules()).start(wait = true)
    //io.ktor.server.cio.EngineMain.main(args)
}
