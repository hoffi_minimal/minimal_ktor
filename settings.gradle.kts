rootProject.name = "minimal_ktor"

include(":apiModel")
include(":backend")
include(":cli")
include(":scratch")
