package com.hoffi.ktor.env

expect object EnvInfo {
    fun exeType(): String
    fun osName(): String
    fun cpuArch(): String
}
