package com.hoffi.ktor.env

actual object EnvInfo {
    actual fun exeType() = "Native"

    actual fun osName() = "${Platform.osFamily}"

    actual fun cpuArch() = "${Platform.cpuArchitecture}"
}
