package main

import com.hoffi.ktor.cli.App

fun main(args: Array<String>) {
    println("Hello, Kotlin/Native!")
    App().main(args)
}

actual fun <T : HttpClientEngineConfig> configureHttpClientEngine(engineFactory: HttpClientEngineFactory<T>): HttpClientConfig<T>.() -> Unit = {}
