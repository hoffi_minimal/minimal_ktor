package main

import io.ktor.server.engine.*
import io.ktor.server.netty.*
import kotlinx.serialization.ExperimentalSerializationApi
import java.io.File
import java.security.KeyStore

@ExperimentalSerializationApi
fun main() {
    println("Hello, classic JVM! in ${System.getProperty("user.dir")}")
    val environment = applicationEngineEnvironment {
        connector {
            port = 8080
        }
        sslConnector(
            KeyStore.getInstance(File("./keystore.jks"), "password".toCharArray()),
            "minimalKtorAlias",
            { "password".toCharArray() },
            { "password".toCharArray() }) {
            port = 8443
            keyStorePath = File("./keystore.jks").absoluteFile
        }
        module(com.hoffi.server.loadModules())
    }

    embeddedServer(Netty, environment).start(wait = true)
//    io.ktor.server.netty.EngineMain.main(args) // using configuration in src/commonMain/resources/application.conf
}

//fun doIt() {
//    val JSON = Json {
//        prettyPrint = true; prettyPrintIndent = " ".repeat(2)
//        serializersModule = this.serializersModule.apply {
//            overwriteWith(
//                SerializersModule {
//                    contextual(Instant::class, InstantSerializer)
//                    contextual(LocalDateTime::class, LocalDateTimeSerializer)
//                }
//            )
//        }
//    }
//
//    val instant = Clock.System.now()
//    val s: String = JSON.encodeToString(instant)
//    println("str:${s.replace("\"", "")}")
//    println("loc:${instant.toLocalDateTime(TimeZone.currentSystemDefault())}")
//    // val i = JSON.decodeFromString<Instant>(InstantSerializer, s)
//    val i = JSON.decodeFromString<Instant>(s)
//    println("ins:$i")
//    println("utc:${i.toLocalDateTime(TimeZone.UTC)}")
//    // val l = JSON.decodeFromString<LocalDateTime>(LocalDateTimeSerializer, s)
//    val l = JSON.decodeFromString<LocalDateTime>(s)
//    println("ldt:$l")
//}
