package main

import com.hoffi.ktor.cli.App
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.serialization.ExperimentalSerializationApi

@ExperimentalSerializationApi
@ExperimentalCoroutinesApi
fun main(args: Array<String>) {
    println("Hello, Kotlin/Native!")
    App().main(args)
}
