package com.hoffi.ktor.cli

import com.github.ajalt.clikt.core.CliktCommand
import com.hoffi.ktor.apiModel.ApiData
import com.hoffi.ktor.apiRoutes.module3.Module3Routes
import com.hoffi.ktor.cli.json.IpInfo
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.plugins.logging.*
import io.ktor.client.plugins.resources.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.coroutines.*
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import mu.KotlinLogging
import okio.FileSystem


expect val exeType: String
expect val fileSystem: FileSystem

@ExperimentalCoroutinesApi
@ExperimentalSerializationApi
class App : CliktCommand() {
    private val log = KotlinLogging.logger {}
    //val count: Int by option(help="Number of greetings").int().default(3)
    //val name: String by option(help="The person to greet").prompt("Your name")

    override fun run() {
//        echo("echo '--count ${count} times '--name=${name}':")
//        (1..count).forEach { i ->
//            echo("${i}: Hello $name!")
//        }

        // After you finish working with the HTTP client, you need to free up the resources: threads, connections, and CoroutineScope.
        // To do this, call the HttpClient.close() method
        val httpClient = HttpClient(CIO) {
            install(Logging) {
                logger = Logger.DEFAULT
                level = LogLevel.HEADERS
            }
            expectSuccess = true
            engine {
                if (exeType != "Native") {
                    https {
                        configureTLS()
                    }
                }
            }
            install(ContentNegotiation) {
                json(Json {
                    prettyPrint = true
                    prettyPrintIndent = " ".repeat(2)
                    isLenient = true
                    ignoreUnknownKeys = true
                })
            }
            install(Resources)
            defaultRequest {
                host = "0.0.0.0"
                port = 8080
                url { protocol = URLProtocol.HTTP }
            }
        }

        // use() will autoclose the httpClient
        //val status = httpClient.use { client -> }
        runBlocking {
            val request: Deferred<HttpResponse> = GlobalScope.async {
                //httpClient.request("https://ipinfo.io/json") {
                httpClient.request("https://ipinfo.io/json") {
                    method = HttpMethod.Get
                    headers {
                        append(HttpHeaders.Accept, "application/json")
                    }
                    url {
                        if (exeType != "Native") {
                            protocol = URLProtocol.HTTPS
                        } else {
                            protocol = URLProtocol.HTTP
                        }
                        host = "ipinfo.io"
                        path("json")
                    }
                }
            }
            request.await()

            // body as txt
            val stringBody: String = request.getCompleted().bodyAsText()
            println(stringBody)

            // body as class instance via ContentNegotiation
            val ipInfo: IpInfo = request.getCompleted().body()
            println(ipInfo)

            // some nice output to the terminal
            println()
            val anyRegex = ".".toRegex()
            val msg = "=====   You obviously live in ${ipInfo.city}! ( https://www.google.com/maps/@${ipInfo.loc},11z )   ====="
            val underline = msg.replace(anyRegex, "=")
            println(underline)
            println(msg)
            println(underline)

            println()
            println()
            println("making type-safe ktor request via apiModel:")
            // type-safe request via apiRoutes
            val apiData: ApiData = httpClient.get(Module3Routes.Data()).body()
            println()
            println(apiData)
        }

        // don't forget to close the httpClient when finished with it
        httpClient.close()
    }
}
