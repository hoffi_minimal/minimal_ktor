plugins {
    kotlin("jvm") version BuildSrcGlobal.VersionKotlin
    kotlin("multiplatform") version BuildSrcGlobal.VersionKotlin apply false
    kotlin("plugin.serialization") version BuildSrcGlobal.VersionKotlin apply false
    id("com.github.johnrengelman.shadow") version "shadow".pluginVersion() apply false
}

group = "com.hoffi"
version = "1.0.0"
val theMainClass by extra { "main/MainKt" }

allprojects {
    //println("> root/build.gradle.kts allprojects: $project")
    repositories {
        mavenCentral()
    }
}

subprojects {
    //println("> root/build.gradle.kts subprojects for: sub$project")
    pluginManager.withPlugin("org.jetbrains.kotlin.jvm") {
        println("root/build.gradle.kts subprojects {}: configuring sub$project as kotlin(\"jvm\") project")
    }
    pluginManager.withPlugin("org.jetbrains.kotlin.multiplatform") {
        println("root/build.gradle.kts subprojects {}: configuring sub$project as kotlin(\"multiplatform\") project")
    }
}

// Helper tasks to speed up things and don't waste time
//=====================================================
// 'c'ompile 'c'ommon
val cc by tasks.registering {
    dependsOn(":compileKotlinJvm", ":compileTestKotlinJvm")
}
val ca by tasks.registering {
    dependsOn(cc)
    dependsOn(":compileKotlinNative", ":compileTestKotlinNative")
}

// ################################################################################################
// #####    pure informational stuff on stdout    #################################################
// ################################################################################################
tasks.register<CheckVersionsTask>("checkVersions") { // implemented in buildSrc/src/main/kotlin/CheckVersionsTask.kt
    scope = "ALL"
}
tasks.register("printClasspath") {
    group = "misc"
    description = "print classpath"
    doLast {
        //project.getConfigurations().filter { it.isCanBeResolved }.forEach {
        //    println(it.name)
        //}
        //println()
        val targets = listOf(
            "metadataCommonMainCompileClasspath",
            "commonMainApiDependenciesMetadata",
            "commonMainImplementationDependenciesMetadata",
            "jvmCompileClasspath",
            "kotlinCompilerClasspath"
        )
        targets.forEach { targetConfiguration ->
            println("$targetConfiguration:")
            println("=".repeat("$targetConfiguration:".length))
            project.getConfigurations()
                .getByName(targetConfiguration).files
                // filters only existing and non-empty dirs
                .filter { (it.isDirectory() && it.listFiles().isNotEmpty()) || it.isFile() }
                .forEach { println(it) }
        }
    }
}
