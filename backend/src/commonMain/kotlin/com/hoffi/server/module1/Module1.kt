package com.hoffi.server.module1

import com.hoffi.server.fileSystem
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import okio.Path.Companion.toPath

fun Application.module1() {
    routing {
        get("/module1") {
            val fileContent = fileSystem.read("backend/src/commonMain/kotlin/com/hoffi/server/Server.kt".toPath()) { readUtf8() }
            call.respondText(fileContent)
        }
    }
}
