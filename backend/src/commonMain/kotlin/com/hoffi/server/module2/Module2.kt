package com.hoffi.server.module2

import com.hoffi.ktor.apiModel.ApiData
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kotlinx.datetime.Clock
import kotlinx.datetime.TimeZone
import kotlinx.datetime.toLocalDateTime
import kotlinx.serialization.ExperimentalSerializationApi

@ExperimentalSerializationApi
fun Application.module2() {
    routing {
        get("/module2") {
            //call.respondText("Hello from 'module2' ($exeType)")
            call.respond(ApiData("hardcoded", Clock.System.now().toLocalDateTime(TimeZone.currentSystemDefault())))
        }
    }
}
