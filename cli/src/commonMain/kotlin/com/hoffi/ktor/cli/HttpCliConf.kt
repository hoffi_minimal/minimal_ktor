package com.hoffi.ktor.cli

import io.ktor.network.tls.*

expect fun configureTLS(): TLSConfigBuilder.() -> Unit
