package com.hoffi.server

import com.hoffi.ktor.json.serializers.InstantSerializer
import com.hoffi.ktor.json.serializers.LocalDateTimeSerializer
import com.hoffi.server.module1.module1
import com.hoffi.server.module2.module2
import com.hoffi.server.module3.module3
import com.hoffi.server.module4.module4
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.resources.*
import kotlinx.datetime.Instant
import kotlinx.datetime.LocalDateTime
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.json.Json
import kotlinx.serialization.modules.SerializersModule
import kotlinx.serialization.modules.overwriteWith
import okio.FileSystem

expect val exeType: String
expect val fileSystem: FileSystem

/** used for embeddedServer */
@ExperimentalSerializationApi
fun loadModules(): Application.() -> Unit = {
    install(Resources)
    install(ContentNegotiation) {
        json(Json {
            prettyPrint = true; prettyPrintIndent = " ".repeat(2)
            serializersModule = this.serializersModule.apply {
                overwriteWith(
                    SerializersModule {
                        contextual(Instant::class, InstantSerializer)
                        contextual(LocalDateTime::class, LocalDateTimeSerializer)
                    }
                )
            }
        })
    }
    module1()
    module2()
    module3()
    module4()
}
