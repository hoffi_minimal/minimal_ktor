package com.hoffi.server.module4

import com.hoffi.ktor.apiModel.ApiData
import com.hoffi.server.fileSystem
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import okio.Path.Companion.toPath

@ExperimentalSerializationApi
fun Application.module4() {
    routing {
        get("/module4") {
            val fileContents = fileSystem.read("data/apiData.json".toPath()) { readUtf8() }
            val apiData = Json.decodeFromString<ApiData>(fileContents)
            call.respond(apiData)
        }
    }
}
