package com.hoffi.ktor.env

actual object EnvInfo {
    actual fun exeType() = "JVM"

    actual fun osName() = System.getProperty("os.name")

    actual fun cpuArch() = System.getProperty("os.arch")
}
