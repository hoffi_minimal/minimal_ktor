plugins {
    kotlin("multiplatform") version BuildSrcGlobal.VersionKotlin
    kotlin("plugin.serialization")
}

group = "com.hoffi"
version = rootProject.version
val artifactName by extra { rootProject.name.toLowerCase().replace('_', '-') + "_" + project.name.toLowerCase().replace('_', '-') }

kotlin {
    jvm {
        testRuns["test"].executionTask.configure {
            useJUnit()
        }
    }
    when (BuildSrcGlobal.hostOS) {
        BuildSrcGlobal.HOSTOS.MAC -> macosX64()
        BuildSrcGlobal.HOSTOS.LINUX -> linuxX64()
        BuildSrcGlobal.HOSTOS.WINDOWS -> mingwX64()
        else -> throw GradleException("Host OS is not supported in Kotlin/Native: ${BuildSrcGlobal.hostOS} from '${System.getProperty("os.name")}'")
    }
    sourceSets {
        val commonMain by getting { // predefined by gradle multiplatform plugin
            dependencies {
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json".depAndVersion())
                implementation("org.jetbrains.kotlinx:kotlinx-datetime".depAndVersion())
                implementation("com.benasher44:uuid".depAndVersion())

                implementation("io.ktor:ktor-resources".depButVersionOf("io.ktor:ktor-server-core"))
            }
        }
        val commonTest by getting {
            dependencies {
            }
        }
        val jvmMain by getting {
            dependencies {
                runtimeOnly("ch.qos.logback:logback-classic") { version { strictly("ch.qos.logback:logback-classic".depVersionOnly()) } }
            }
        }
        val jvmTest by getting {
            dependencies {
                implementation("io.kotest:kotest-runner-junit5".depAndVersion())
                implementation("io.kotest:kotest-assertions-core".depButVersionOf("io.kotest:kotest-runner-junit5"))
                //    implementation("org.junit.jupiter:junit-jupiter-api:5.6.0")
                //    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
            }
        }
        when (BuildSrcGlobal.hostOS) {
            BuildSrcGlobal.HOSTOS.MAC -> {
                val macosX64Main by getting {
                    dependencies {
                    }
                }
            }
            BuildSrcGlobal.HOSTOS.LINUX -> {
                val linuxX64Main by getting {
                    dependencies {
                    }
                }
            }
            BuildSrcGlobal.HOSTOS.WINDOWS -> {
                val mingwX64Main by getting {
                    dependencies {
                    }
                }
            }
            else -> throw GradleException("Host OS is not supported in Kotlin/Native.")
        }
    }
}

tasks {
    withType<Jar> {
        archiveBaseName.set(artifactName)
    }
}
