plugins {
    kotlin("jvm") version BuildSrcGlobal.VersionKotlin
}

kotlin {
    jvmToolchain(BuildSrcGlobal.jdkVersion)
}
